import pandas as pd
from numpy import random as rdm
from sqlalchemy import create_engine, text, exc

engine = create_engine("postgresql://kiem@:5432/dbwikifantasy")

try:

    co = engine.connect()

    print("Connexion à la base de données : dbWikiFantasy\n")

    df = pd.read_csv('dataset.csv').drop_duplicates().copy()

    dfQuote = df[['quote', 'title']].drop_duplicates().copy()

    dfSupport = df[['title', 'type', 'year']].drop_duplicates().copy()

    # Support data insertion
    for line in dfSupport.itertuples():
        co.execute (text('''INSERT INTO Support (title, form, release)
                            VALUES (:title, :form, :release);'''),
                        {
                            'title'         : line.title,
                            'form'          : line.type,
                            'release'       : line.year
                        })

    co.commit()

    for line in dfQuote.itertuples():

        idSupport = co.execute (
            text('''SELECT id_support, title FROM Support
                    WHERE title = :title;'''),
                {'title' : line.title}
        ).fetchone()

        if idSupport :
            co.execute (
                text('''INSERT INTO Quote (content, img_path, likes, time_code, langue, is_valid, support) 
                        VALUES (:content, :img_path, :likes, :time_code, :langue, :is_valid, :support);'''),
                        {
                            'content'       : line.quote,
                            'img_path'      : "/IMG/PATH/" + (idSupport[1][0:10]).strip().replace(" ", "").replace(":", ""),
                            'likes'         : rdm.randint(1000),
                            'time_code'     : str(rdm.randint(200)) + ":" + str(rdm.randint(59)),
                            'langue'        : "English",
                            'is_valid'      : True,
                            'support'       : idSupport[0],
                        })
                    
    co.commit()

except exc.SQLAlchemyError as error:
    print(error)


finally:

    if co is not None:

        print("Déconnexion de la base : dbsae2_04")
        co.close()
