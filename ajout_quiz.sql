INSERT INTO Question (texte, answerA, answerB, answerC, answerD, cAnswer)
VALUES
('May the Force be with you.', 'Star Wars', 'Star Trek', 'The Matrix', 'Blade Runner', ''Star Wars''),
('I m gonna make him an offer he can t refuse.', 'The Godfather', 'Goodfellas', 'Scarface', 'The Sopranos', 'The Godfather'),
('To infinity and beyond!', 'Toy Story', 'The Incredibles', 'Finding Nemo', 'Shrek', 'Toy Story'),
('All we have to decide is what to do with the time that is given to us.', 'The Lord of the Rings', 'Harry Potter', 'The Hobbit', 'Narnia', 'The Lord of the Rings'),
('I am your father.', 'Star Wars', 'The Terminator', 'Star Trek', 'Guardians of the Galaxy', 'Star Wars'),
('It is a trap!', 'Star Wars', 'Battlestar Galactica', 'Star Trek', 'The Matrix', 'Star Wars'),
('Winter is coming.', 'Game of Thrones', 'The Witcher', 'Vikings', 'The Mandalorian', 'Game of Thrones'),
('Elementary, my dear Watson.', 'Sherlock Holmes', 'Agatha Christie', 'Poirot', 'The Hound of the Baskervilles', 'Sherlock Holmes'),
('Here is looking at you, kid.', 'Casablanca', 'Gone with the Wind', 'Citizen Kane', 'The Maltese Falcon', 'Casablanca');


-- INSERTION TABLE Quiz_Question
INSERT INTO Quiz_Question (quiz, question)
VALUES
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(2, 16),
(2, 17),
(2, 18),
(2, 19);