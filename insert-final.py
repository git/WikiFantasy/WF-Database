import os

def replace_apostrophe(text):
    """Remplacer les apostrophes simples par des apostrophes typographiques droites"""
    return text.replace("'", "’")

def extract_data_from_txt(file_path):
    sources = set()  # Pour stocker les œuvres (title, year)
    characters = set()  # Pour stocker les personnages (character, img_path)
    images = set()  # Pour stocker les chemins d'images
    quotes = []  # Pour stocker les citations (quote, oeuvre, personnage, année, chemin d'image)

    with open(file_path, 'r', encoding='utf-8') as file:
        for line in file:
            if line.strip():  # Ignore les lignes vides
                data = line.split(";")
                if len(data) >= 5:  # Vérifie que la ligne a bien toutes les colonnes
                    quote = replace_apostrophe(data[0].strip())  # Citation
                    oeuvre = replace_apostrophe(data[1].strip())  # Œuvre
                    character = replace_apostrophe(data[2].strip())  # Personnage
                    year = data[3].strip()  # Année de parution
                    img_path = data[4].strip()  # Chemin de l'image

                    sources.add((oeuvre, year))
                    characters.add((character, img_path))
                    images.add(img_path)
                    quotes.append((quote, oeuvre, character, year, img_path))

    return list(sources), list(characters), list(images), quotes

def create_sql_insert_file(sources, characters, images, quotes, output_sql_file):
    with open(output_sql_file, 'w', encoding='utf-8') as file:
        # Insertion des images dans la table Image
        file.write("-- Insertion des images dans la table Image\n")
        image_id_map = {}
        for i, img_path in enumerate(images, start=1):
            file.write(f"INSERT INTO Image (id_img, imgPath) VALUES ({i}, '{img_path}');\n")
            image_id_map[img_path] = i  # Stocker l'id généré pour l'image

        # Insertion des œuvres dans la table Source
        file.write("\n-- Insertion des œuvres dans la table Source\n")
        source_id_map = {}
        for i, (oeuvre, year) in enumerate(sources, start=1):
            file.write(f"INSERT INTO Source (id_source, title, dateS) VALUES ({i}, '{oeuvre}', {year});\n")
            source_id_map[oeuvre] = i  # Stocker l'id généré pour l'œuvre

        # Insertion des personnages dans la table Caracter
        file.write("\n-- Insertion des personnages dans la table Caracter\n")
        character_id_map = {}
        for j, (character, img_path) in enumerate(characters, start=1):
            id_img = image_id_map[img_path]  # Associer l'image au personnage
            file.write(f"INSERT INTO Caracter (id_caracter, caracter, id_img) VALUES ({j}, '{character}', {id_img});\n")
            character_id_map[character] = j  # Stocker l'id généré pour le personnage

        # Insertion des citations dans la table Quote
        file.write("\n-- Insertion des citations dans la table Quote\n")
        for k, (quote, oeuvre, character, year, img_path) in enumerate(quotes, start=1):
            id_source = source_id_map[oeuvre]  # Récupérer l'id_source de l'œuvre
            id_caracter = character_id_map[character]  # Récupérer l'id_caracter du personnage
            file.write(f"INSERT INTO Quote (id_quote, content, likes, langue, isValide, reason, id_caracter, id_source, id_user_verif) "
                       f"VALUES ('{k}', '{quote}', 0, 'fr', false, 'insertion de test', {id_caracter}, {id_source}, 'U001');\n")

    print(f"Fichier SQL créé : {output_sql_file}")

# Remplacer "chemin_vers_ton_fichier_txt" par le chemin de ton fichier
txt_file_path = 'citation.txt'
output_sql_path = 'insert_all_data.sql'

# Extraire les données (sources, personnages, images, citations) du fichier texte
sources, characters, images, quotes = extract_data_from_txt(txt_file_path)

# Créer le fichier .sql avec les insertions
create_sql_insert_file(sources, characters, images, quotes, output_sql_path)
