-- INSERTION TABLE IMAGE
INSERT INTO Image (imgPath)
VALUES
('/images/photo1.jpg'),
('/images/photo2.png'),
('/images/photo3.gif'),
('/images/photo4.jpg'),
('/images/photo5.png'),
('/images/photo6.gif'),
('/images/photo7.jpg'),
('/images/photo8.png'),
('/images/photo9.gif'),
('/images/photo10.jpg');


-- INSERTION TABLE USERS
INSERT INTO Users (id_user, username, email, pssword, img, creation)
VALUES
('U001', 'admin', 'admin@example.com', 'password1', 1, '2024-10-01'),
('U002', 'user2', 'user2@example.com', 'password2', 2, '2024-10-02'),
('U003', 'user3', 'user3@example.com', 'password3', 3, '2024-10-03'),
('U004', 'user4', 'user4@example.com', 'password4', 4, '2024-10-04'),
('U005', 'user5', 'user5@example.com', 'password5', 5, '2024-10-05'),
('U006', 'user6', 'user6@example.com', 'password6', 6, '2024-10-06'),
('U007', 'user7', 'user7@example.com', 'password7', 7, '2024-10-07'),
('U008', 'user8', 'user8@example.com', 'password8', 8, '2024-10-08'),
('U009', 'user9', 'user9@example.com', 'password9', 9, '2024-10-09'),
('U010', 'user10', 'user10@example.com', 'password10', 10, '2024-10-10');



-- INSERTION TABLE USERS
INSERT INTO Admin (users) VALUES ('U001');


-- INSERTION TABLE QUESTION
INSERT INTO Question (texte, answerA, answerB, answerC, answerD, cAnswer)
VALUES
('May the Force be with you.', 'Star Wars', 'Star Trek', 'The Matrix', 'Blade Runner', 'Star Wars'),
('I m gonna make him an offer he can t refuse.', 'The Godfather', 'Goodfellas', 'Scarface', 'The Sopranos', 'The Godfather'),
('To infinity and beyond!', 'Toy Story', 'The Incredibles', 'Finding Nemo', 'Shrek', 'Toy Story'),
('All we have to decide is what to do with the time that is given to us.', 'The Lord of the Rings', 'Harry Potter', 'The Hobbit', 'Narnia', 'The Lord of the Rings'),
('I am your father.', 'Star Wars', 'The Terminator', 'Star Trek', 'Guardians of the Galaxy', 'Star Wars'),
('It is a trap!', 'Star Wars', 'Battlestar Galactica', 'Star Trek', 'The Matrix', 'Star Wars'),
('Winter is coming.', 'Game of Thrones', 'The Witcher', 'Vikings', 'The Mandalorian', 'Game of Thrones'),
('Elementary, my dear Watson.', 'Sherlock Holmes', 'Agatha Christie', 'Poirot', 'The Hound of the Baskervilles', 'Sherlock Holmes'),
('Here is looking at you, kid.', 'Casablanca', 'Gone with the Wind', 'Citizen Kane', 'The Maltese Falcon', 'Casablanca'),
('I see dead people.', 'The Sixth Sense', 'The Others', 'Poltergeist', 'The Shining', 'The Sixth Sense');

-- INSERTION TABLE Quiz
INSERT INTO Quiz (levelQ, timer)
VALUES
(1, 30),
(2, 45),
(3, 60),
(1, 20),
(2, 50),
(3, 90),
(4, 120),
(5, 150),
(1, 15),
(2, 40);


-- INSERTION TABLE Quiz_Question
INSERT INTO Quiz_Question (quiz, question)
VALUES
(1, 1),  -- Quiz 1, Question 1
(1, 2),  -- Quiz 1, Question 2
(1, 3),  -- Quiz 1, Question 3
(2, 1),  -- Quiz 2, Question 1
(2, 4),  -- Quiz 2, Question 4
(3, 2),  -- Quiz 3, Question 2
(3, 5),  -- Quiz 3, Question 5
(4, 6),  -- Quiz 4, Question 6
(5, 3),  -- Quiz 5, Question 3
(5, 7);  -- Quiz 5, Question 7


-- INSERTION TABLE Record_quiz
INSERT INTO Record_quiz (users, quiz, nbPoint, timeQ)
VALUES
('U001', 1, 10, 25),
('U002', 2, 15, 40),
('U003', 3, 20, 30),
('U004', 1, 5, 20),
('U005', 5, 18, 60),
('U006', 2, 12, 35),
('U007', 4, 25, 50),
('U008', 3, 0, 10),
('U009', 1, 14, 45),
('U010', 5, 20, 55);


-- INSERTION TABLE Source
INSERT INTO Source (title, dateS)
VALUES
('The Great Gatsby', '1925-04-10'),
('To Kill a Mockingbird', '1960-07-11'),
('1984', '1949-06-08'),
('Pride and Prejudice', '1813-01-28'),
('The Catcher in the Rye', '1951-07-16'),
('Moby Dick', '1851-10-18'),
('War and Peace', '1869-01-01'),
('The Odyssey', '1320-01-01'),
('The Divine Comedy', '1320-01-01'),
('The Brothers Karamazov', '1880-11-01');


-- INSERTION TABLE Caracter
INSERT INTO Caracter (caracter, img_path)
VALUES
('Harry Potter', '/images/characters/harry_potter.jpg'),
('Hermione Granger', '/images/characters/hermione_granger.jpg'),
('Ron Weasley', '/images/characters/ron_weasley.jpg'),
('Frodo Baggins', '/images/characters/frodo_baggins.jpg'),
('Samwise Gamgee', '/images/characters/samwise_gamgee.jpg'),
('Gandalf', '/images/characters/gandalf.jpg'),
('Luke Skywalker', '/images/characters/luke_skywalker.jpg'),
('Darth Vader', '/images/characters/darth_vader.jpg'),
('Sherlock Holmes', '/images/characters/sherlock_holmes.jpg'),
('Katniss Everdeen', '/images/characters/katniss_everdeen.jpg');


-- INSERTION TABLE Quote
INSERT INTO Quote (id_quote, content, likes, langue, isValide, reason, id_caracter, id_source, id_user_verif)
VALUES
('Q001', 'Not all those who wander are lost.', 10, 'en', true, 'Inspirational quote', 1, 1, 'U001'),
('Q002', 'To be, or not to be, that is the question.', 15, 'en', true, 'Famous Shakespeare quote', 2, 2, 'U002'),
('Q003', 'C est la vie.', 5, 'fr', false, 'Incomplete context', 3, 3, 'U003'),
('Q004', 'All you need is love.', 20, 'en', true, 'Beatles quote', 4, 4, 'U004'),
('Q005', 'La vie est un défi, relève-le!', 8, 'fr', true, 'Motivational quote', 5, 5, 'U005'),
('Q006', 'To infinity and beyond!', 12, 'en', true, 'Toy Story quote', 6, 6, 'U006'),
('Q007', 'Veni, vidi, vici.', 18, 'fr', false, 'Historical reference', 7, 7, 'U007'),
('Q008', 'The only thing we have to fear is fear itself.', 25, 'en', true, 'Franklin D. Roosevelt', 8, 8, 'U008'),
('Q009', 'L imagination est plus importante que la connaissance.', 30, 'fr', true, 'Einstein quote', 9, 9, 'U009'),
('Q010', 'In three words I can sum up everything I ve learned about life: it goes on.', 22, 'en', true, 'Robert Frost', 10, 10, 'U010');

-- INSERTION TABLE Favorite
INSERT INTO Favorite (users, quote)
VALUES
('U001', 'Q001'),  -- User U001 favorites Quote Q001
('U002', 'Q002'),  -- User U002 favorites Quote Q002
('U003', 'Q003'),  -- User U003 favorites Quote Q003
('U001', 'Q004'),  -- User U001 also favorites Quote Q004
('U004', 'Q001'),  -- User U004 favorites Quote Q001
('U005', 'Q005'),  -- User U005 favorites Quote Q005
('U006', 'Q002'),  -- User U006 favorites Quote Q002
('U007', 'Q007'),  -- User U007 favorites Quote Q007
('U008', 'Q008'),  -- User U008 favorites Quote Q008
('U009', 'Q009');  -- User U009 favorites Quote Q009



-- INSERTION TABLE Commentary
INSERT INTO Commentary (id_comment, quote, users, dateC, comment)
VALUES
('C00001', 'Q001', 'U001', '2024-10-01', 'This quote really resonates with me.'),
('C00002', 'Q002', 'U002', '2024-10-02', 'A timeless piece of wisdom.'),
('C00003', 'Q003', 'U003', '2024-10-03', 'I disagree with this viewpoint.'),
('C00004', 'Q004', 'U001', '2024-10-04', 'Absolutely love this quote!'),
('C00005', 'Q005', 'U004', '2024-10-05', 'Very motivational, thanks for sharing!'),
('C00006', 'Q006', 'U005', '2024-10-06', 'This makes me reflect on life.'),
('C00007', 'Q007', 'U006', '2024-10-07', 'I find this quote very inspiring.'),
('C00008', 'Q008', 'U007', '2024-10-08', 'I think there’s more to this than meets the eye.'),
('C00009', 'Q009', 'U008', '2024-10-09', 'Such a powerful statement!'),
('C00010', 'Q010', 'U009', '2024-10-10', 'This is one of my favorites!');


